import csv
from typing import List
from urllib.parse import urljoin
from urllib.request import urlopen
from bs4 import BeautifulSoup
from csv import DictWriter

from lecture import Lecture


def get_lecture_list(is_master: bool = True) -> List[Lecture]:
    if is_master:
        lv_site = "https://hpi.de/studium/lehrveranstaltungen/it-systems-engineering-ma.html"
    else:
        lv_site = "https://hpi.de/studium/lehrveranstaltungen/it-systems-engineering-ba.html"

    site_html = urlopen(lv_site)
    soup = BeautifulSoup(site_html, "html.parser")

    content_tables = soup.findAll("table", attrs={"class": ["contenttable", "contenttable-0", "table"]})
    table = content_tables[0]

    lectures = []

    for row in table.findChildren(['tr']):
        lecture_link = row.find("a", attrs={"class": "courselink"})
        lecture_link_target = urljoin(lv_site, lecture_link['href'])
        lecture_name = lecture_link.contents[0].strip()
        prof = row.findAll("a")[1].contents[0]
        lectures.append(Lecture(lecture_link_target, lecture_name, prof))

    return lectures


def write_csv(lecture_list: List[Lecture]):
    lecture_list = [lecture.fill_self() for lecture in lecture_list]

    with open('lectures.csv', 'w', newline="") as csv_file:
        field_names = ["name", "prof", "link", "sws", "ects", "appointments", "modules"]
        writer = csv.DictWriter(csv_file, fieldnames=field_names, dialect="excel-tab", extrasaction="ignore")
        writer.writeheader()
        for lecture in lecture_list:
            writer.writerow(lecture.to_dict())


if __name__ == "__main__":
    lecture_list = get_lecture_list(is_master=True)
    #lecture_list[1].fill_self()

    write_csv(lecture_list)



