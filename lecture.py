import json
from difflib import SequenceMatcher
from itertools import takewhile
from typing import Dict, List
from urllib.request import urlopen

from bs4 import BeautifulSoup, NavigableString, Tag


class Lecture:
    def __init__(self, link, name, prof):
        self.name = name
        self.link = link
        self.prof = prof
        self.requirements = None
        self.description = None
        self.literature = None
        self.teaching_format = None
        self.learning_teaching_format = None
        self.appointments = None
        self.sws = None
        self.ects = None
        self.power_recording = None
        self.graded = None
        self.maximum_attendees = None
        self.modules : str = ""

    def to_dict(self) -> Dict:
        return self.__dict__

    def to_json(self) -> str:
        return json.dumps(self.__dict__, indent=2, sort_keys=True)

    def fill_self(self):
        site_html = urlopen(self.link)
        soup = BeautifulSoup(site_html, "html.parser")
        lecture_block = soup.find("div", attrs={"class": "tx-ciuniversity-course"})

        headered_content = self.__get_headered_content(lecture_block)

        for header, content in headered_content.items():
            if self.is_similar(header, "Beschreibung"):
                self.description = self.__join_html(content)
            elif self.is_similar(header, "Voraussetzungen"):
                self.requirements = self.__join_html(content)
            elif self.is_similar(header, "Literatur"):
                self.literature = self.__join_html(content)
            elif self.is_similar(header, "Lern- und Lehrformen"):
                self.learning_teaching_format = self.__join_html(content)
            elif self.is_similar(header, "Leistungserfassung"):
                self.power_recording = self.__join_html(content)
            elif self.is_similar(header, "Termine"):
                self.appointments = self.__join_html(content)
            elif self.is_similar(header, "Allgemeine Information"):
                self.__parse_common_info([content_item for content_item in content if isinstance(content_item, Tag)][0])
            elif self.is_similar(header, "Studiengänge & Module") or self.is_similar(header, "Studiengänge &amp; Module") or self.is_similar(header, "Module"):
                self.__parse_modules(content)

        return self

    def __parse_common_info(self, content):
        list_items = self.__html_to_python_list(content)

        for item in list_items:
            item_description = str(item[0]).split(":")[0]
            item_value = str(item[0]).split(":")[1]

            if self.is_similar(item_description, "Semesterwochenstunden"):
                self.sws = int(item_value)
            elif self.is_similar(item_description, "ECTS"):
                self.ects = int(item_value)
            elif self.is_similar(item_description, "Benotet"):
                pass
            elif self.is_similar(item_description, "Einschreibefrist"):
                pass
            elif self.is_similar(item_description, "Programm"):
                pass
            elif self.is_similar(item_description, "Lehrform"):
                self.teaching_format = item_value
            elif self.is_similar(item_description, "Belegungsart"):
                pass
            elif self.is_similar(item_description, "Maximale Teilnehmerzahl"):
                self.maximum_attendees = int(item_value)

    def __parse_modules(self, content):
        content = filter(lambda el: isinstance(el, Tag), content)
        content = filter(
            lambda el: self.is_similar(
                el.find("div", attrs={"class": "tx_dscclipclap_header"}).get_text().strip(),
                "IT-Systems Engineering MA"
            ),
            content)

        try:
            modules = list(content)[0].find_all("ul", attrs={"class": "tx_dscclipclap_content"})
            self.modules = ", ".join([module.get_text().strip() for module in modules])
        except IndexError as e:
            pass

    @staticmethod
    def __html_to_python_list(list):
        result = []
        for list_item in list.findAll("li"):
            result.append(list_item.contents)
        return result

    @staticmethod
    def is_similar(a, b) -> bool:
        similarity_threshold = 0.8
        return SequenceMatcher(None, a, b).ratio() >= similarity_threshold

    @staticmethod
    def __join_html(contents: List) -> str:
        result = ""
        for el in contents:
            if callable(getattr(el, 'get_text', None)):
                result += el.get_text()
            else:
                result += str(el)
        return result

    @staticmethod
    def __get_headered_content(lecture_block) -> Dict[str, List]:
        # https://stackoverflow.com/a/8735688
        headers = lecture_block.findAll("h2")
        result_dict = {}

        for header, next_header in zip(headers, headers[1:]):
            # elements between header and next_header
            between_it = takewhile(lambda el: el is not next_header, header.nextSiblingGenerator())
            # iterate over those
            result_dict[header.text] = list(between_it)

        last_header_elements = list(takewhile(lambda el: "Zurück" not in str(el), headers[-1].nextSiblingGenerator()))
        result_dict[headers[-1].text] = last_header_elements

        return result_dict
